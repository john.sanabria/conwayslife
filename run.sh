#!/usr/bin/env bash
# 
# Script para correr jupyter notebook.
# Tomado de: https://github.com/jupyter/docker-stacks
#
# Fecha: 10-11-2022
#
# MODIFICADO
#
# 16-05-2023   Se hace ACTUALIZACION DE IMAGEN de Jupyter. Una vez actualizada
#              la imagen se hace la ejecucion del script '/pip/run_pip.sh' para
#              instalar las librerias. Se crea una nueva imagen a partir de 
#              este contenedor 
#              'docker container commit jupyter-all josanabr/datascience-notebook:2023-05-16_v01'
# 15-05-2023   Se instalo el paquete 'pandasai' -> josanabr/datascience-notebook:2023-05-15_v01
# 05-05-2023   Se actualizo la fecha del contenedor -> josanabr/datascience-notebook:2023-05-05_v01
#
#docker run -it --name jupyter-all --rm -p 8888:8888  -v "$(pwd)/pip":/pip -v "$(pwd)/work":/home/jovyan/work josanabr/datascience-notebook:2023-05-16_v01
docker run -it --name jupyter-all --rm -p 8888:8888  -v "$(pwd)/pip":/pip -v "$(pwd)/work":/home/jovyan/work jupyter/datascience-notebook
