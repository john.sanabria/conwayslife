# Entorno para la ejecución del juego de la vida de Conway en contenedores

En este repositorio encontrará los archivos necesarios para ejecutar el algoritmo del juego de la vida de Conway usando un entorno de Jupyter Notebook bajo __Linux__.

Al clonar este repositorio usted tendrá un directorio llamado __conwayslife__.
Ingrese al directorio y ejecute el comando `./run.sh`.
En un momento de la ejecución del script este mostrará una cadena similar a esta `http://127.0.0.1:8888/lab?token=`, cópiela y péguela en su navegador.

En el panel de navegación encontrado en la parte izquierda de la página web recien cargada ubíquese en el directorio `work/Parallelism` y abra el Notebook `Multithreading_Multiprocessing_Juego_De_La_Vida.ipynb`. 

De manera preliminar ejecute las diferentes celdas que permiten la ejecución del juego de la vida de Conway en sus diferentes versiones.
`¿Observa algo similar a lo que observó en clase respecto a los tiempos de ejecución de las diferentes versiones?`

## Tarea

En clase les indiqué que deberían hacer una modificación a la configuración inicial de la matriz donde se desarrolla el juego de la vida de Conway de manera que tuviera una configuración particular. En la versión con la que usted cuenta inicialmente los valores de la "matriz" son generados aleatoriamente (buscar línea de código con la función `np.random.choice()`).
Para la tarea esta configuración no debe ser aleatoria sino particular.
Aquí la configuración particular que debiera tener.

![Configuración inicial del juego de la vida de Conway](images/config_inicial.png)

Esta configuración es tomada del sitio [playgameoflife](https://playgameoflife.com/).

Usando el Jupyter Notebook de este repositorio, modifique la celda que tiene el comentario `Version MULTIPROCESSING del juego de la vida de Conway con ANIMACIÓN` y logre un comportamiento del algoritmo similar al que se observa en el sitio [playgameoflife](https://playgameoflife.com/).
Para ello se sugiere llevar a cabo al menos los siguientes pasos.

1. Preparar la configuración inicial del tablero del juego de la vida de Conway como lo muestra la figura anterior. No es importante la ubicación exacta pero sí que solo un número pequeño de celdas este activa y ubicadas en aproximadamente el centro del "tablero". Recuerde que activadas quiere decir que tienen valor de 1.

2. Modificar las líneas de código que se encuentran en la función `conway()` de manera que se comporte de acuerdo a estas reglas.
![Reglas particulares del juego de la vida de Conway](images/rules.png).
